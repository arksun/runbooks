groups:
- name: GitLab Saturation Ratios
  interval: 1m
  rules:
  # type: *, component: cpu
  # this measures average CPU across all the cores for the entire fleet for the given service
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'cpu'
    expr: >
      avg by (type, tier, stage, environment) (
        instance:node_cpu_utilization:ratio{type!="", type!~"console-node|deploy-node"}
      )

  # type: *, component: single_node_cpu
  # this measures the maximum cpu availability across all the codes on a single server
  # this can be helpful if CPU is not even distributed across the fleet.
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'single_node_cpu'
    expr: >
      max(
        avg by (fqdn, type, tier, stage, environment) (
          instance:node_cpu_utilization:ratio{type!="", type!~"console-node|deploy-node"}
        )
      ) without (fqdn)

  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_space'
    expr: >
      max by (type, tier, stage, environment) (
        1 -
        instance:node_filesystem_avail:ratio{type!="",tier!="",fstype=~"ext.|xfs"}
      )

  - record: gitlab_component_saturation:ratio
    labels:
      component: 'memory'
    expr: >
      max by (type, tier, stage, environment) (
        instance:node_memory_utilization:ratio{type!="",type!="monitoring"}
      )

  - record: gitlab_component_saturation:ratio
    labels:
      component: 'go_memory'
    expr: >
      max by (type, tier, stage, environment) (
        sum by (fqdn, type, tier, stage, environment) (
          go_memstats_alloc_bytes{type=~"web|git|api|gitaly|web-pages|monitoring|praefect"}
        ) /
        sum by (fqdn, type, tier, stage, environment) (
          node_memory_MemTotal_bytes{type=~"web|git|api|gitaly|web-pages|monitoring|praefect"}
        )
      )

  # type: web/api/git, component: workers
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'workers'
    expr: >
      clamp_max(
        sum(avg_over_time(unicorn_active_connections{job=~"gitlab-(rails|unicorn)"}[1m])) by (type, tier, stage, environment)
        /
        sum(max(unicorn_workers) without (pid)) by (type, tier, stage, environment),
        1
      )

  # type: *, component: pod_count
  # This measures the HPA that manages our Deployments. If we are running low on
  # ability to scale up by hitting our maximum HPA Pod allowance, we will have
  # fully saturated this service.
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'pod_count'
    expr: >
      avg_over_time(kube_hpa_status_current_replicas[1m])
      /
      avg_over_time(kube_hpa_spec_max_replicas[1m])

  # type: web/api/git, component: single_node_unicorn_workers
  # Single node workers is a similar metric to the normal unicorn `worker` metric,
  # except that it only measures a single host. This reason for this is that once HAProxy has routed
  # a request to a single host it is (for now) trapped on that host until its complete.
  # There are cases where a single host will receive an unbalanced amount of traffic. In this case,
  # unicorn saturation could occur on that host without happening across the fleet in general.
  # An example of this was https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7916
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'single_node_unicorn_workers'
    expr: >
      clamp_max(
        max(
          sum(avg_over_time(unicorn_active_connections{job=~"gitlab-(rails|unicorn)"}[1m])) by (fqdn, type, tier, stage, environment)
          /
          sum(max(unicorn_workers) without (pid)) by (fqdn, type, tier, stage, environment)
        ) without(fqdn),
        1
      )

  # type: web/api/git, component: single_node_puma_workers
  # Single node workers is a similar metric to the normal puma `worker` metric,
  # except that it only measures a single host. This reason for this is that once HAProxy has routed
  # a request to a single host it is (for now) trapped on that host until its complete.
  # There are cases where a single host will receive an unbalanced amount of traffic. In this case,
  # puma saturation could occur on that host without happening across the fleet in general.
  # An example of this was https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7916
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'single_node_puma_workers'
    expr: >
      clamp_max(
        max(
          sum by(fqdn, type, tier, stage, environment) (avg_over_time(puma_active_connections[1m]))
          /
          sum by(fqdn, type, tier, stage, environment) (puma_max_threads{pid="puma_master"})
        ) without(fqdn),
        1
      )

  # type: elasticsearch, component: elastic_cpu
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'elastic_cpu'
    expr: >
      avg by (type, tier, stage, environment) (
          elasticsearch_os_cpu_percent/100
        )

  # type: elasticsearch, component: single_node_cpu_usage
  # this measures the maximum cpu saturation across all the nodes on a single server
  # this can be helpful if CPU is not even distributed across the fleet.
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'elastic_single_node_cpu'
    expr: >
      max(
        avg by (name, type, tier, stage, environment) (
            elasticsearch_os_cpu_percent/100
        )
      ) without (name)

  # type: elasticsearch, component: elastic_disk_space
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'elastic_disk_space'
    expr: >
      max by (type, tier, stage, environment) (
        (elasticsearch_filesystem_data_size_bytes - elasticsearch_filesystem_data_free_bytes) / elasticsearch_filesystem_data_size_bytes
      )

  # type: elasticsearch, component: elastic_jvm_heap_memory
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'elastic_jvm_heap_memory'
    expr: >
      max by (type, tier, stage, environment) (
        elasticsearch_jvm_memory_used_bytes{area="heap"} / elasticsearch_jvm_memory_max_bytes{area="heap"}
      )

  # type: redis/redis cache, component: single_threaded_cpu
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'single_threaded_cpu'
    expr: >
      clamp_max(
        max by (type, tier, stage, environment) (
          instance:redis_cpu_usage:rate1m
        ),
        1
      )

  # type: pgbouncer,patroni, component: pgbouncer_single_core
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'pgbouncer_single_core'
    expr: >
      max(
        sum(
          rate(
            namedprocess_namegroup_cpu_seconds_total{groupname=~"pgbouncer.*", type=~"pgbouncer|patroni"}[1m]
          )
        ) by (groupname, fqdn, type, tier, stage, environment)
      ) by (type, tier, stage, environment)

  # type: pgbouncer, component: pgbouncer_sync_pool
  #
  # The sync pool is very sensitive to saturation, since
  # delays obtaining a database connection can lead to
  # 503s and downstream saturation on unicorn/puma workers
  # leading to "mini-outages"
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'pgbouncer_sync_pool'
    expr: >
      max(
        (
          pgbouncer_pools_server_active_connections{user="gitlab", database="gitlabhq_production"} +
          pgbouncer_pools_server_testing_connections{user="gitlab", database="gitlabhq_production"} +
          pgbouncer_pools_server_used_connections{user="gitlab", database="gitlabhq_production"} +
          pgbouncer_pools_server_login_connections{user="gitlab", database="gitlabhq_production"}
        )
        /
        on(environment, tier, type, stage, fqdn, instance) group_left()
        sum by (environment, tier, type, stage, fqdn, instance) (
          pgbouncer_databases_pool_size{name="gitlabhq_production"}
        )
      ) by (type, tier, stage, environment)

  # type: pgbouncer, component: pgbouncer_async_pool
  # Async pool is more resiliant to saturation, since
  # its used by Sidekiq background jobs and some extra
  # delay should not have a major impact on performance
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'pgbouncer_async_pool'
    expr: >
      max(
        (
          pgbouncer_pools_server_active_connections{user="gitlab", database="gitlabhq_production_sidekiq"} +
          pgbouncer_pools_server_testing_connections{user="gitlab", database="gitlabhq_production_sidekiq"} +
          pgbouncer_pools_server_used_connections{user="gitlab", database="gitlabhq_production_sidekiq"} +
          pgbouncer_pools_server_login_connections{user="gitlab", database="gitlabhq_production_sidekiq"}
        )
        /
        on(environment, tier, type, stage, fqdn, instance) group_left()
        sum by (environment, tier, type, stage, fqdn, instance) (
          pgbouncer_databases_pool_size{name="gitlabhq_production_sidekiq"}
        )
      ) by (type, tier, stage, environment)

  # type: postgres-delayed, postgres-archive, patroni
  # postgres has a MAX_CONNECTIONS value which specifies the maximum number of active connections
  # the server can handle concurrently. This records the number of non-idle connections as a
  # fraction of the maximum. When we reach saturation
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'active_db_connections'
    expr: >
      clamp_max(
        max(
          sum without (state) (
            pg_stat_activity_count{datname="gitlabhq_production", state!="idle"}
          )
          /  on (type, tier, stage, environment, fqdn)
          pg_settings_max_connections
      ) by (type, tier, stage, environment), 1)

  # type: redis, redis-cache, component: redis_memory
  # Records Redis memory usage
  # TODO: After upgrading to Redis 4, we should include the rdb_last_cow_size in this value
  # so that we include the RDB snapshot utilization in our memory usage
  # See https://gitlab.com/gitlab-org/omnibus-gitlab/issues/3785#note_234689504
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'redis_memory'
    expr: >
      max(
        max(
          label_replace(redis_memory_used_rss_bytes, "memtype", "rss","","")
          or
          label_replace(redis_memory_used_bytes, "memtype", "used","","")
        ) by (type, tier, stage, environment, fqdn)
        / on(fqdn) group_left
        node_memory_MemTotal_bytes
      )  without (fqdn)

  # type: redis, redis-cache, component: redis_clients
  # Records the saturation of redis client connections against a redis fleet
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'redis_clients'
    expr: >
      max(
        max_over_time(redis_connected_clients[1m])
        /
        redis_config_maxclients
      ) by (environment, tier, type, stage)

  # type: *
  # cadvisor cgroup memory saturation limits
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'cgroup_memory'
    expr: >
      max(
        (
          container_memory_usage_bytes{id="/system.slice/gitlab-runsvdir.service"} -
          container_memory_cache{id="/system.slice/gitlab-runsvdir.service"} -
          container_memory_swap{id="/system.slice/gitlab-runsvdir.service"}
        )
        /
        container_spec_memory_limit_bytes{id="/system.slice/gitlab-runsvdir.service"}
      ) by (environment, tier, type, stage)

  # type: gitaly
  # Disk read IOPS saturation based on GCP estimate of 60000 IO operations/second
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/file-35-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_read_iops'
    expr: >
      clamp_max(
        max(
          rate(node_disk_reads_completed_total{type="gitaly", device="sdb"}[1m]) / (60000)
        ) by (environment, tier, type, stage),
        1
      )

  # type: gitaly
  # Disk write IOPS saturation based on GCP estimate of 30000 IO operations/second
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/file-35-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_write_iops'
    expr: >
      clamp_max(
        max(
          rate(node_disk_writes_completed_total{type="gitaly", device="sdb"}[1m]) / (30000)
        ) by (environment, tier, type, stage),
        1
      )

  # type: gitaly
  # Disk read throughput saturation based on GCP estimate of 1200MB/s limit
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/file-35-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_read_throughput'
    expr: >
      clamp_max(
        max(
          rate(node_disk_read_bytes_total{type="gitaly", device="sdb"}[1m]) / (1200 * 1024 * 1024)
        ) by (environment, tier, type, stage),
        1
      )

  # type: gitaly
  # Disk write throughput saturation based on GCP estimate of 400MB/s limit
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/file-35-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_write_throughput'
    expr: >
      clamp_max(
        max(
          rate(node_disk_written_bytes_total{type="gitaly", device="sdb"}[1m]) / (400 * 1024 * 1024)
        ) by (environment, tier, type, stage),
        1
      )

  # type: nfs
  # Disk read IOPS saturation based on GCP estimate of 15000 IO operations/second
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-c/disks/share-01-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_read_iops'
    expr: >
      clamp_max(
        max(
          rate(node_disk_reads_completed_total{type="nfs", device="sdb"}[1m]) / (15000)
        ) by (environment, tier, type, stage),
        1
      )

  # type: nfs
  # Disk write IOPS saturation based on GCP estimate of 15000 IO operations/second
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-c/disks/share-01-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_write_iops'
    expr: >
      clamp_max(
        max(
          rate(node_disk_writes_completed_total{type="nfs", device="sdb"}[1m]) / (15000)
        ) by (environment, tier, type, stage),
        1
      )

  # type: nfs
  # Disk write throughput saturation based on GCP estimate of 400MB/s limit
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-c/disks/share-01-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_write_throughput'
    expr: >
      clamp_max(
        max(
          rate(node_disk_written_bytes_total{type="nfs", device="sdb"}[1m]) / (400 * 1024 * 1024)
        ) by (environment, tier, type, stage),
        1
      )

  # type: nfs
  # Disk read throughput saturation based on GCP estimate of 800MB/s limit
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-c/disks/share-01-stor-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_read_throughput'
    expr: >
      clamp_max(
        max(
          rate(node_disk_read_bytes_total{type="nfs", device="sdb"}[1m]) / (800 * 1024 * 1024)
        ) by (environment, tier, type, stage),
        1
      )

  # type: patroni
  # Disk read IOPS saturation based on GCP estimate of 100000 IO operations/second
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/patroni-02-db-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_read_iops'
    expr: >
      clamp_max(
        max(
          rate(node_disk_reads_completed_total{type="patroni", device="sdb"}[1m]) / (100000)
        ) by (environment, tier, type, stage),
        1
      )

  # type: patroni
  # Disk write IOPS saturation based on GCP estimate of 30000 IO operations/second
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/patroni-02-db-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_write_iops'
    expr: >
      clamp_max(
        max(
          rate(node_disk_writes_completed_total{type="patroni", device="sdb"}[1m]) / (30000)
        ) by (environment, tier, type, stage),
        1
      )

  # type: patroni
  # Disk write throughput saturation based on GCP estimate of 800MB/s limit
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/patroni-02-db-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_write_throughput'
    expr: >
      clamp_max(
        max(
          rate(node_disk_written_bytes_total{type="patroni", device="sdb"}[1m]) / (800 * 1024 * 1024)
        ) by (environment, tier, type, stage),
        1
      )

  # type: patroni
  # Disk read throughput saturation based on GCP estimate of 1200MB/s limit
  # This value can be found in the GCP console, here https://console.cloud.google.com/compute/disksDetail/zones/us-east1-d/disks/patroni-02-db-gprd-data?project=gitlab-production
  # For now, we hard-code the device as `sdb`
  # Note: please update `magic_numbers.libsonnet` when updating this value!
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'disk_sustained_read_throughput'
    expr: >
      clamp_max(
        max(
          rate(node_disk_read_bytes_total{type="patroni", device="sdb"}[1m]) / (1200 * 1024 * 1024)
        ) by (environment, tier, type, stage),
        1
      )

  # Open file descriptors
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'open_fds'
    expr: >
      max(
        label_replace(
          max_over_time(process_open_fds[1m])
          /
          max_over_time(process_max_fds[1m])
          , "client", "general", "", ""
        )
        or
        label_replace(
          max_over_time(ruby_file_descriptors[1m])
          /
          max_over_time(ruby_process_max_fds[1m])
          , "client", "ruby", "", ""
        )
      ) by (environment, type, tier, stage)

  # ci-runners: shared_runners/shared_runners_gitlab/private_runners
  # Each runner manager has a maximum number of CI jobs that it can handle at any one time
  # when this limit is reached, new jobs need to be queued.
  # Defaults to using the environment `gprd` since runners do not always
  # have the correct environment label
  - record: gitlab_component_saturation:ratio
    labels:
      type: ci-runners
      tier: runners
      stage: main
    expr: >
      label_replace(
        clamp_max(
          max by(environment, component) (
            sum without (state, executor_stage, exported_stage) (
              label_replace(max_over_time(gitlab_runner_jobs{job="shared-runners"}[1m]), "component", "shared_runners", "", "")
              or
              label_replace(max_over_time(gitlab_runner_jobs{job="shared-runners-gitlab-org"}[1m]), "component", "shared_runners_gitlab", "", "")
              or
              label_replace(max_over_time(gitlab_runner_jobs{job="private-runners"}[1m]), "component", "private_runners", "", "")
            )
            /
            (
              (
                label_replace(max_over_time(gitlab_runner_limit{job="shared-runners"}[1m]), "component", "shared_runners", "", "")
                or
                label_replace(max_over_time(gitlab_runner_limit{job="shared-runners-gitlab-org"}[1m]), "component", "shared_runners_gitlab", "", "")
                or
                label_replace(max_over_time(gitlab_runner_limit{job="private-runners"}[1m]), "component", "private_runners", "", "")
              )
              > 0
            )
          ),
          1
        ),
        "environment", "gprd", "environment", ""
      )

  # sidekiq: sidekiq_workers
  #
  # Sidekiq worker saturation refers to a situation where all workers that are available to process a
  # subset of jobs are busy processing those jobs. This leads to queuing of new jobs. For background
  # processing, this is sometimes accecptable over short periods, but generally not over longer
  # periods.
  - record: gitlab_component_saturation:ratio
    labels:
      component: 'sidekiq_workers'
    expr: >
      clamp_max(
        max by(environment, tier, type, stage) (
          sum by (fqdn, instance, environment, tier, type, stage) (sidekiq_running_jobs{priority!="export"})
          /
          sum by (fqdn, instance, environment, tier, type, stage) (sidekiq_concurrency{priority!="export"})
        ),
        1
      )

  # Aggregate over all components within a service using max
  # DEPRECATED!
  - record: gitlab_service_saturation:ratio
    expr: >
      max by (environment, tier, type, stage) (gitlab_component_saturation:ratio)

# Unlike other service metrics, we record the stats for each component independently
- name: GitLab Saturation Ratios Stats
  interval: 5m
  rules:

  # ----------------------------------------------
  # Linear Interpolation
  # ----------------------------------------------

  # Average values for each component, over a week
  - record: gitlab_component_saturation:ratio:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_component_saturation:ratio[1w])

  # Stddev for each component, over a week
  - record: gitlab_component_saturation:ratio:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_component_saturation:ratio[1w])

  # Using linear week-on-week growth, what prediction to we have for 2w from now?
  - record: gitlab_component_saturation:ratio:predict_linear_2w
    expr: >
      predict_linear(gitlab_component_saturation:ratio:avg_over_time_1w[1w], 86400 * 14)

  # Using linear week-on-week growth, what prediction to we have for 30d from now?
  - record: gitlab_component_saturation:ratio:predict_linear_30d
    expr: >
      predict_linear(gitlab_component_saturation:ratio:avg_over_time_1w[1w], 86400 * 30)
