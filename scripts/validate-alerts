#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'logger'

LOGGER = Logger.new(STDERR)
LOGGER.level = Logger::DEBUG

# rubocop:disable Metrics/PerceivedComplexity
# rubocop:disable Metrics/CyclomaticComplexity
def validate_rule(alert_file_path, rule)
  return if rule["record"] # Don't validate recordings

  annotations = rule["annotations"]
  labels = rule["labels"]
  alert = rule["alert"]

  raise StandardError, "Rules must contain an `alert` attribute" unless alert

  raise StandardError, "#{alert}: rules must contain a `title` annotation" unless annotations["title"]

  LOGGER.warn "#{alert_file_path}: #{alert}: Rules should contain a `description` annotation" unless annotations["description"]

  raise StandardError, " #{alert}: rules must contain a `severity` label" unless labels["severity"]
  raise StandardError, " #{alert}: rules contains an invalid `severity` label: #{labels['severity']}" unless %w[s1 s2 s3 s4].include?(labels["severity"])

  if labels["pager"]
    raise StandardError, " #{alert}: rules contains an invalid `pager` label: #{labels['pager']}" unless %w[pagerduty issue].include?(labels["pager"])
    raise StandardError, " #{alert}: only severity s1 and s2 errors should page" unless (labels["severity"] == "s1") || (labels["severity"] == "s2")
    raise StandardError, " #{alert}: 'pager: issue' alerts need a project label" if (labels["pager"] == "issue") && labels["project"].nil?
  elsif (labels["severity"] == "s1") || (labels["severity"] == "s2")
    raise StandardError, " #{alert}: s1 and s2 alerts must be configured to send to pagerduty"
  end

  raise StandardError, " #{alert}: alerts must include an alert_type label. This label should either be 'cause' or 'symptom'. For more information on cause and symptom-based alerts, review http://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/#symptoms-versus-causes-g0sEi4" unless %w[cause symptom].include?(labels["alert_type"])

  if annotations["runbook"]
    runbook_relative_link = annotations["runbook"]
    runbook_file = runbook_relative_link.gsub(/#.*$/, "")
    # We can't validate files that include template values eg: "{{ $labels.type }}"
    # but for other files, ensure that they exist
    raise StandardError, " #{alert}: rule references a non-existent runbook #{runbook_file}" if !runbook_file.include?("{{") && !File.file?(File.join(__dir__, "..", runbook_file))
  else
    LOGGER.warn "#{alert_file_path}: #{alert}: alerts should reference a runbook"
  end
end
# rubocop:enable Metrics/PerceivedComplexity
# rubocop:enable Metrics/CyclomaticComplexity

def validate_group(alert_file_path, group)
  name = group["name"]

  begin
    rules = group["rules"]

    rules.each do |rule|
      validate_rule alert_file_path, rule
    end
  rescue StandardError => e
    raise StandardError, "group `#{name}`: #{e.message}"
  end
end

def validate(alert_file_path)
  alert_yaml = YAML.load_file(alert_file_path)
  raise StandardError, "Invalid configuration" unless alert_yaml

  groups = alert_yaml["groups"]
  groups.each do |group|
    validate_group alert_file_path, group
  end
rescue StandardError => e
  raise StandardError, "Unable to validate file #{alert_file_path}: #{e.message}"
end

begin
  alert_files = File.join(__dir__, "..", "rules", "*.yml")
  Dir[alert_files].each do |file|
    validate File.expand_path(file)
  end

rescue StandardError => e
  warn "error: #{e.message}"
  exit 1
ensure
  LOGGER.close
end
